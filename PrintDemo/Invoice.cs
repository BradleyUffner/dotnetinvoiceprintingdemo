﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDemo
{
    class Invoice
    {
        public Invoice()
        {
            CompanyName = "Widget Co.";
            PreviousBalance = 0;
            Payments = new List<Payment>();
            LineItems = new List<LineItem>();
        }

        public string CompanyName { get; }
        public decimal PreviousBalance { get; }
        public List<Payment> Payments { get; }
        public List<LineItem> LineItems { get; }
    }
}
