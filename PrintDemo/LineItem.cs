﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDemo
{
    class LineItem
    {
        public DateTime Date { get; set; }
        public string Description { get; set; }

        public decimal Price { get; set; }
    }
}
