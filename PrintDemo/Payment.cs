﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintDemo
{
    class Payment
    {
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }
}
