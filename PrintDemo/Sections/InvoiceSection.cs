﻿using System.Drawing;
using System.Drawing.Printing;

namespace PrintDemo.Sections
{
    abstract class InvoiceSection
    {
        protected InvoiceSection(InvoiceDocument invoiceDocument)
        {
            this.InvoiceDocument = invoiceDocument;
        }

        public InvoiceDocument InvoiceDocument { get; }

        public abstract void Render(PrintPageEventArgs e);

        public Invoice Invoice => InvoiceDocument?.Invoice;
    }
}
