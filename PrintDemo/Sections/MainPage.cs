﻿using System.Drawing;
using System.Drawing.Printing;

namespace PrintDemo.Sections
{
    internal class MainPage : InvoiceSection
    {

        public MainPage(InvoiceDocument invoiceDocument) : base(invoiceDocument) { }

        public override void Render(PrintPageEventArgs e)
        {
            e.Graphics.FillEllipse(Brushes.Green, e.MarginBounds.Left, e.MarginBounds.Top, e.MarginBounds.Left + 100, e.MarginBounds.Top + 100);
            e.Graphics.DrawString(Invoice.CompanyName, InvoiceDocument.TitleFont, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + 30);

            e.HasMorePages = true;
            InvoiceDocument.ChangeSection(new SummmarySection(InvoiceDocument));
        }
    }
}